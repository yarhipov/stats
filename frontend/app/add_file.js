var fileCtrl = angular.module('fileControllers', []);

fileCtrl.controller('addFileCtrl', ['$scope', '$http',
    function ($scope, $http) {
        var load_files = function(){
            $http.get('/data/list_files/').success(function(data){
                $scope.files = data;
            });
        };

        load_files();

        $scope.complete = function(data){
            load_files();
        };
  }]);