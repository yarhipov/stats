var statsApp = angular.module(
    'statsApp',
    [
        'ngRoute',
        'ui.bootstrap',
        'fileControllers',
        'ngUpload',
        'ngCookies',
    ]);

statsApp.config(['$routeProvider',
    function($routeProvider){
        $routeProvider.
            when('/add_file',{
                templateUrl: '/stats/app/templates/add_file.html',
                controller: 'addFileCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });

    }]);

statsApp.run(function($rootScope, $http, $cookies){
    $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

});

