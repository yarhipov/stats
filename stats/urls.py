from django.conf.urls import patterns, include, url

from django.contrib import admin
from statsapp import views

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^data/save_file/', views.save_file),
    url(r'^data/list_files/', views.list_files),
)
