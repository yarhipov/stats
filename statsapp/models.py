from django.db import models


class UploadedFile(models.Model):
    filename = models.TextField()
    filetype = models.CharField(max_length=100)
    network = models.CharField(max_length=100)


class Formulas(models.Model):
    name = models.TextField()
    formula = models.TextField()
