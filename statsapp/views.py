import json
from os.path import basename

from django.http import HttpResponse
from django.conf import settings

from statsapp.models import UploadedFile

from rest_framework.decorators import api_view
from rest_framework.response import Response


def handle_uploaded_file(files):
    path = settings.STATICFILES_DIRS[0]
    result = []
    for f in files:
        filename = '/xml/'.join([path, f.name])
        destination = open(filename, 'wb+')
        for chunk in f.chunks():
            destination.write(chunk)
        destination.close()
        result.append(filename)
    return result


def save_file(request):
    file_path = handle_uploaded_file(request.FILES.getlist('uploaded_file'))[0]
    filename = basename(file_path)
    UploadedFile.objects.create(filename=filename, network='', filetype='')
    data = dict()
    json_data = json.dumps(data)
    return HttpResponse(json_data, content_type='application/json')

@api_view(['GET', ])
def list_files(request):
    data = []
    for f in UploadedFile.objects.all():
        data.append(dict(
            filename=f.filename,
            network=f.network,
            filetype=f.filetype))
    return Response(data)

